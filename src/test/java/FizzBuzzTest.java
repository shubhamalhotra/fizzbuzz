import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzTest {

    @Test
    public void shouldReturn1WhentTheNumberIs1()
    {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.convert(1);
        assertEquals("1", result);
    }

    @Test
    public void shouldReturn2WhentTheNumberIs2()
    {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.convert(2);
        assertEquals("2", result);
    }

    @Test
    public void shouldReturnFizzWhenNumberIs3()
    {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.convert(3);
        assertEquals("Fizz",result);
    }

    @Test
    public void shouldReturnFizzWhenNumberIsMultipleOf3()
    {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.convert(6);
        assertEquals("Fizz",result);
    }

    @Test
    public void shouldReturnBuzzWhenNumberIs5()
    {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.convert(5);
        assertEquals("Buzz",result);
    }

    @Test
    public void shouldReturnBuzzWhenNumberIsMultipleOf5()
    {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.convert(10);
        assertEquals("Buzz",result);
    }

    @Test
    public void shouldReturnFizzBuzzWhenNumberIsMultipleOfBoth3And5()
    {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.convert(15);
        assertEquals("FizzBuzz",result);
    }

    @Test
    public void shouldReturnSameNumberWhenNumberIsNotMultipleOfBoth3And5()
    {
        FizzBuzz fizzBuzz = new FizzBuzz();
        String result = fizzBuzz.convert(16);
        assertEquals("16",result);
    }
}